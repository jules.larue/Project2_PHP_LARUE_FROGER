<?php
	class Movie{
		private $film_id;
		private $title;
		private $genre;
		private $poster_path; // The link to get the movie poster
		private $director;
		private $year;
		private $release_date;
		private $runtime;
		private $country;
		private $synopsis;
		private $actors;
		private $imdb_rating;
		private $insertion_datetime;

		function __construct($film_id, $title, $genre, $poster_path, $director, $year, $release_date, $runtime, $country, $synopsis, $actors, $imdb_rating, $insertion_datetime){
			/*
				The constructor of a movie.
			*/
			$this->film_id=$film_id;
			$this->title=$title;
			$this->genre=$genre;
			$this->poster_path=$poster_path;
			$this->director=$director;
			$this->year=$year;
			$this->release_date=$release_date;
			$this->runtime=$runtime;
			$this->country=$country;
			$this->synopsis=$synopsis;
			$this->actors=$actors;
			$this->imdb_rating=$imdb_rating;
			$this->insertion_datetime=$insertion_datetime;
		}


		/*
			All the getters
		*/

		public function getFilmid(){
			return $this->film_id;
		}

		public function getTitle(){
			return $this->title;
		}

		public function getGenre(){
			return $this->genre;
		}

		public function getPosterPath(){
			return $this->poster_path;
		}

		public function getDirector(){
			return $this->director;
		}

		public function getYear(){
			return $this->year;
		}

		public function getReleaseDate(){
			return $this->release_date;
		}

		public function getRuntime(){
			return $this->runtime;
		}

		public function getCountry(){
			return $this->country;
		}

		public function getSynopsis(){
			return $this->synopsis;
		}

		public function getActors(){
			return $this->actors;
		}

		public function getImdbRating(){
			return $this->imdb_rating;
		}

		public function getInsertionDateTime(){
			return $this->insertion_datetime;
		}


	}

?>