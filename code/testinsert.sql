/*
JEU DE TESTS POUR LE SITE 
*/
-- Supprimer les insertions existantes
delete from ACTEURS;
delete from FILMS;
delete from GENRES;
delete from INDIVIDUS;


-- Pour réinitialiser l'incrémentation à 1
ALTER TABLE GENRES AUTO_INCREMENT = 1;
ALTER TABLE INDIVIDUS AUTO_INCREMENT = 1;
ALTER TABLE FILMS AUTO_INCREMENT = 1;
ALTER TABLE ACTEURS AUTO_INCREMENT = 1;

insert into GENRES(`name`) values('Action');
insert into GENRES(`name`) values('Adventure');
insert into GENRES(`name`) values('Animation');
insert into GENRES(`name`) values('Comedy');
insert into GENRES(`name`) values('Drama');
insert into GENRES(`name`) values('Science fiction');

insert into INDIVIDUS(`lastname`, `firstname`) values('Bell', 'Kristen');
insert into INDIVIDUS(`lastname`, `firstname`) values('Menzel', 'Idina');
insert into INDIVIDUS(`lastname`, `firstname`) values('Groff', 'Jonathan');
insert into INDIVIDUS(`lastname`, `firstname`) values('Gad', 'Josh');
insert into INDIVIDUS(`lastname`, `firstname`) values('Buck', 'Chris');

insert into INDIVIDUS(`lastname`, `firstname`) values('Docter', 'Pete');
insert into INDIVIDUS(`lastname`, `firstname`) values('Hader', 'Bill');
insert into INDIVIDUS(`lastname`, `firstname`) values('Black', 'Lewis');
insert into INDIVIDUS(`lastname`, `firstname`) values('Poehler', 'Amy');

insert into INDIVIDUS(`lastname`, `firstname`) values('Bale', 'Christian');
insert into INDIVIDUS(`lastname`, `firstname`) values('Ledger', 'Heath');
insert into INDIVIDUS(`lastname`, `firstname`) values('Eckhart', 'Aaron');
insert into INDIVIDUS(`lastname`, `firstname`) values('Nolan', 'Christopher');
insert into INDIVIDUS(`lastname`, `firstname`) values('McConaughey', 'Matthew');
insert into INDIVIDUS(`lastname`, `firstname`) values('Hathaway', 'Anne');
insert into INDIVIDUS(`lastname`, `firstname`) values('Silverman', 'David');
insert into INDIVIDUS(`lastname`, `firstname`) values('Castellaneta', 'Dan');
insert into INDIVIDUS(`lastname`, `firstname`) values('Kavner', 'Julie');
insert into INDIVIDUS(`lastname`, `firstname`) values('Cartwright', 'Nancy');
insert into INDIVIDUS(`lastname`, `firstname`) values('McQueen', 'Steve');
insert into INDIVIDUS(`lastname`, `firstname`) values('Ejiofor', 'Chiwetel');
insert into INDIVIDUS(`lastname`, `firstname`) values('Fassbender', 'Michael');
insert into INDIVIDUS(`lastname`, `firstname`) values('Abrams', 'J.J');
insert into INDIVIDUS(`lastname`, `firstname`) values('Ford', 'Harrison');
insert into INDIVIDUS(`lastname`, `firstname`) values('Hamill', 'Mark');
insert into INDIVIDUS(`lastname`, `firstname`) values('Fisher', 'Carrie');
insert into INDIVIDUS(`lastname`, `firstname`) values('Bird', 'Brad');
insert into INDIVIDUS(`lastname`, `firstname`) values('Garrett', 'Brad');
insert into INDIVIDUS(`lastname`, `firstname`) values('Romano', 'Lou');
insert into INDIVIDUS(`lastname`, `firstname`) values('Oswalt', 'Patton');
insert into INDIVIDUS(`lastname`, `firstname`) values('Bendelack', 'Steve');
insert into INDIVIDUS(`lastname`, `firstname`) values('Atkinson', 'Rowan');
insert into INDIVIDUS(`lastname`, `firstname`) values('Dafoe', 'Willem');
insert into INDIVIDUS(`lastname`, `firstname`) values('Pemberton', 'Steve');
insert into INDIVIDUS(`lastname`, `firstname`) values('MacFarlane', 'Seth');
insert into INDIVIDUS(`lastname`, `firstname`) values('Wahlberg', 'Christopher');
insert into INDIVIDUS(`lastname`, `firstname`) values('Kunis', 'Mila');
insert into INDIVIDUS(`lastname`, `firstname`) values('Nima', 'Nourizadeh');
insert into INDIVIDUS(`lastname`, `firstname`) values('Mann', 'Thomas');
insert into INDIVIDUS(`lastname`, `firstname`) values('Cooper', 'Oliver');


insert into FILMS(`title`, `country`, `year`, `release_date`, `runtime`, `insertion_datetime`, `synopsis`, `director_id`, `genre_id`, `poster_path`, `imdb_rating`)  values('Frozen','United States', '2013', '21 Nov 2013', '102', NOW(), 'Synopsis of Frozen blablabla', 5, 3, 'http://ia.media-imdb.com/images/M/MV5BMTQ1MjQwMTE5OF5BMl5BanBnXkFtZTgwNjk3MTcyMDE@._V1_SX300.jpg', 8); -- insertion du film "Frozen"

insert into FILMS(`title`, `country`, `year`, `release_date`, `runtime`, `insertion_datetime`, `synopsis`, `director_id`, `genre_id`, `poster_path`, `imdb_rating`)  values('Inside Out','United States', '2015', '17 June 2015', '94', NOW(), 'Synopsis of Inside Out blablabla', 6, 3, 'http://ia.media-imdb.com/images/M/MV5BOTgxMDQwMDk0OF5BMl5BanBnXkFtZTgwNjU5OTg2NDE@._V1_SX300.jpg', 8);

insert into FILMS(`title`, `country`, `year`, `release_date`, `runtime`, `insertion_datetime`, `synopsis`, `director_id`, `genre_id`, `poster_path`, `imdb_rating`)  values('The Dark Knight','United States', '2008', '17 August 2008', '147', NOW(), 'Synopsis of The Dark Knight blablabla', 13, 1, 'http://ia.media-imdb.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_SX300.jpg', 8);

insert into FILMS(`title`, `country`, `year`, `release_date`, `runtime`, `insertion_datetime`, `synopsis`, `director_id`, `genre_id`, `poster_path`, `imdb_rating`)  values('Interstellar','United States', '2014', '5 Novembre 2014', '169', NOW(), 'A team of explorers travel through a wormhole in space in an attempt to ensure the survival of humanity.', 13, 2, 'http://ia.media-imdb.com/images/M/MV5BMjIxNTU4MzY4MF5BMl5BanBnXkFtZTgwMzM4ODI3MjE@._V1__SX1796_SY899_.jpg', 8);

insert into FILMS(`title`, `country`, `year`, `release_date`, `runtime`, `insertion_datetime`, `synopsis`, `director_id`, `genre_id`, `poster_path`, `imdb_rating`)  values('The Simpsons Movie','United States', '2007', '25 July 2007', '87', NOW(), 'After Homer accidentally pollutes the town\'s water supply, Springfield is encased in a gigantic dome by the EPA and the Simpson family are declared fugitives.', 16, 3, 'http://ia.media-imdb.com/images/M/MV5BMTgxMDczMTA5N15BMl5BanBnXkFtZTcwMzk1MzMzMw@@._V1__SX1796_SY899_.jpg', 8);

insert into FILMS(`title`, `country`, `year`, `release_date`, `runtime`, `insertion_datetime`, `synopsis`, `director_id`, `genre_id`, `poster_path`, `imdb_rating`)  values('12 years a slave','United States', '2013', '22 January 2014', '134', NOW(), 'In the antebellum United States, Solomon Northup, a free black man from upstate New York, is abducted and sold into slavery.', 20, 5, 'http://ia.media-imdb.com/images/M/MV5BMjExMTEzODkyN15BMl5BanBnXkFtZTcwNTU4NTc4OQ@@._V1__SX1796_SY899_.jpg', 8);

insert into FILMS(`title`, `country`, `year`, `release_date`, `runtime`, `insertion_datetime`, `synopsis`, `director_id`, `genre_id`, `poster_path`, `imdb_rating`)  values('Star Wars: Episode VII','United States', '2015', '16 December 2015', '136', NOW(), 'The seventh episode of Georges\' Lucas Saga', 23, 6, 'http://ia.media-imdb.com/images/M/MV5BMTkwNzAwNDA4N15BMl5BanBnXkFtZTgwMTA2MDcwNzE@._V1__SX1796_SY899_.jpg', 8);

insert into FILMS(`title`, `country`, `year`, `release_date`, `runtime`, `insertion_datetime`, `synopsis`, `director_id`, `genre_id`, `poster_path`, `imdb_rating`)  values('Ratatouille','United States', '2007', '1 August 2007', '111', NOW(), 'A rat who can cook makes an unusual alliance with a young kitchen worker at a famous restaurant.', 27, 3, 'http://ia.media-imdb.com/images/M/MV5BMTMzODU0NTkxMF5BMl5BanBnXkFtZTcwMjQ4MzMzMw@@._V1__SX1796_SY899_.jpg', 8);

insert into FILMS(`title`, `country`, `year`, `release_date`, `runtime`, `insertion_datetime`, `synopsis`, `director_id`, `genre_id`, `poster_path`, `imdb_rating`)  values('Mr. Bean\'s holiday','England', '2007', '18 April 2007', '90', NOW(), 'Mr. Bean wins a trip to Cannes where he unwittingly separates a young boy from his father and must help the two come back together. On the way he discovers France, bicycling, and true love, among other things', 31, 4, 'http://ia.media-imdb.com/images/M/MV5BMTM2NzQ1Mzc4M15BMl5BanBnXkFtZTcwNTk3NjA1MQ@@._V1__SX1796_SY899_.jpg', 8);

insert into FILMS(`title`, `country`, `year`, `release_date`, `runtime`, `insertion_datetime`, `synopsis`, `director_id`, `genre_id`, `poster_path`, `imdb_rating`)  values('Ted','United States', '2012', '10 October 2012', '106', NOW(), 'From the creator of Family Guy comes a movie about John Bennett, whose wish of bringing his teddy bear to life came true. Now, John must decide between keeping the relationship with the teddy bear or his girlfriend, Lori.', 35, 4, 'http://ia.media-imdb.com/images/M/MV5BMTQ1OTU0ODcxMV5BMl5BanBnXkFtZTcwOTMxNTUwOA@@._V1__SX1796_SY899_.jpg', 8);

insert into FILMS(`title`, `country`, `year`, `release_date`, `runtime`, `insertion_datetime`, `synopsis`, `director_id`, `genre_id`, `poster_path`, `imdb_rating`)  values('Project X','United States', '2012', '14 March 2012', '88', NOW(), '3 high school seniors throw a birthday party to make a name for themselves. As the night progresses, things spiral out of control as word of the party spreads.', 38, 4, 'http://ia.media-imdb.com/images/M/MV5BMTc1MTk0Njg4OF5BMl5BanBnXkFtZTcwODc0ODkyNw@@._V1__SX1796_SY899_.jpg', 8 	);

-- On lie les acteurs insérés aux films
insert into ACTEURS values(1, 1);
insert into ACTEURS values(1, 2);
insert into ACTEURS values(1, 3);
insert into ACTEURS values(1, 4);

insert into ACTEURS values(2, 7);
insert into ACTEURS values(2, 8);
insert into ACTEURS values(2, 9);

insert into ACTEURS values(3, 10);
insert into ACTEURS values(3, 11);
insert into ACTEURS values(3, 12);

insert into ACTEURS values(4, 14);
insert into ACTEURS values(4, 15);

insert into ACTEURS values(5, 17);
insert into ACTEURS values(5, 18);
insert into ACTEURS values(5, 19);

insert into ACTEURS values(6, 21);
insert into ACTEURS values(6, 22);

insert into ACTEURS values(7, 24);
insert into ACTEURS values(7, 25);
insert into ACTEURS values(7, 26);

insert into ACTEURS values(8, 28);
insert into ACTEURS values(8, 29);
insert into ACTEURS values(8, 30);

insert into ACTEURS values(9, 32);
insert into ACTEURS values(9, 33);
insert into ACTEURS values(9, 34);

insert into ACTEURS values(10, 36);
insert into ACTEURS values(10, 37);

insert into ACTEURS values(11, 39);
insert into ACTEURS values(11, 40);