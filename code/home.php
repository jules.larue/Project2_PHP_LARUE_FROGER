<?php include_once("database_access.php"); ?> <!-- We include the database functions -->

<!DOCTYPE HTML>
    <!-- This is the homepage of the website, where we can see the latest films added. -->
     <html>
     <head>
     <meta charset="UTF-8" >
     <link rel="stylesheet" type="text/css" href="main.css"/>
     <link rel="shortcut icon" href="../ressources/movie_icon.gif"/> <!-- The icon displayed in the tab -->
     <title>The web films collection</title>
     </head>

     <body>
     <?php include_once("header.html"); ?> <!-- We display the header -->


       <?php displayAside(); ?>


     <section id="main_section">
       <h2>Last added</h2>
       <?php displayLastAddedFilms(); ?>
     </section>
     </body>
     </html>
