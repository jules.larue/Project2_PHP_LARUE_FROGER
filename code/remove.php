<?php
include_once("database_access.php");
?>

<script>
var res = window.confirm("Do you really want to remove this film ?");
if (res==true){
    <?php
    removeFilm($_GET['id']); // remove the film
    header("Location: home.php"); // we redirect to home page
    ?>
    alert("The film has been remove from the database."); // we display a confirmation message to user
}
</script>