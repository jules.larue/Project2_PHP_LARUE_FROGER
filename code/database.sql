-- phpMyAdmin SQL Dump
-- version 4.4.2
-- http://www.phpmyadmin.net
--
-- Client :  servinfo-db
-- Généré le :  Ven 17 Avril 2015 à 10:58
-- Version du serveur :  5.5.41-MariaDB-1ubuntu0.14.04.1
-- Version de PHP :  5.5.9-1ubuntu4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

-- --------------------------------------------------------

use dbgfroger;


DROP TABLE IF EXISTS `ACTEURS`;
DROP TABLE IF EXISTS `FILMS`;
DROP TABLE IF EXISTS `INDIVIDUS`;
DROP TABLE IF EXISTS `GENRES`;
--
-- Structure de la table `genres`
--


CREATE TABLE `GENRES` (
  `genre_id` int(3) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Structure de la table `individus`
--


CREATE TABLE `INDIVIDUS` (
  `indiv_id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `lastname` varchar(20) DEFAULT NULL,
  `firstname` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Structure de la table `films`
--


CREATE TABLE `FILMS` (
  `film_id` int(6) PRIMARY KEY AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `country` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `release_date` varchar(20) DEFAULT NULL,
  `runtime` int(3) DEFAULT NULL,
  `insertion_datetime` datetime DEFAULT NULL,
  `synopsis` varchar(500) DEFAULT NULL,
  `director_id` int(4),
  `genre_id` int(3),
  `poster_path` varchar(300) DEFAULT NULL,
  `imdb_rating` decimal(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `FILMS` ADD FOREIGN KEY (`director_id`) REFERENCES `INDIVIDUS`(`indiv_id`);
ALTER TABLE `FILMS` ADD FOREIGN KEY (`genre_id`) REFERENCES `GENRES`(`genre_id`);

--
-- Structure de la table `acteurs`
--


CREATE TABLE `ACTEURS` (
  `ref_id_film` int(6),
  `ref_id_acteur` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `ACTEURS` ADD FOREIGN KEY (`ref_id_film`) REFERENCES `FILMS`(`film_id`);
ALTER TABLE `ACTEURS` ADD FOREIGN KEY (`ref_id_acteur`) REFERENCES `INDIVIDUS`(`indiv_id`);




/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
