<?php include_once("database_access.php"); session_start(); // used for error message ?> <!-- We include the database functions -->


<!DOCTYPE HTML>
    <!-- This is the homepage of the website, where we can see the latest films added. -->
     <html>
     <head>
     <meta charset="UTF-8" >
     <link rel="stylesheet" type="text/css" href="main.css"/>
     <link rel="stylesheet" type="text/css" href="addGenre.css"/>
     <link rel="shortcut icon" href="../ressources/movie_icon.gif"/> <!-- The icon displayed in the tab -->
     <title>The web films collection</title>
     </head>

     <body>
     <?php include_once("header.html"); ?> <!-- We display the header -->

       <?php displayAside(); ?> 



     <form name="formAddGenre" action="actionAddGenre.php" method="POST"/>
     <p>What genre would you like to add ?</p>
     <input id="genre" type="text" name="genrefield" required />
     <input id="submitGenre" type="submit" value="Add it!" />
     </form>

     <?php if(isset($_SESSION['error-genre'])){
                echo $_SESSION['error-genre'];
                unset($_SESSION['error-genre']); // we destroy the variable
     }
     ?>

     </body>
     </html>
