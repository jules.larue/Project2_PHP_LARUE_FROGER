<?php include_once("database_access.php"); 


?> 

<!DOCTYPE HTML>
    <!-- This is the homepage of the website, where we can see the latest films added. -->
     <html>
     <head>
     <meta charset="UTF-8" >
     <link rel="stylesheet" type="text/css" href="main.css"/>
     <link rel="shortcut icon" href="../ressources/movie_icon.gif"/> <!-- The icon displayed in the tab -->
     <title>The web films collection</title>
     </head>

     <body>
     <?php include_once("header.html"); ?> <!-- We display the header -->


       <?php displayAside(); ?>


     <section id="main_section">
       <?php 
        if(isset($_GET['genre']))
            echo "<h2>All ".$_GET['genre']." movies </h2>";
        else
            echo "<h2>All movies released in ".$_GET['year']."</h2>";
            ?>

       
       <?php displaySearchFilterResult(); ?>
     </section>
     </body>
     </html>