<?php include_once("database_access.php"); // We include the database functions
include_once("countries.php");
function generateRateList(){
    echo "<div id=\"ratefield\" >";
    echo "<h3>Your rate</h3>";
    echo "<select name=\"rate_select\">"; // default selected value is 5
    foreach(range(10, 0, -1) as $i){
        echo "<option value=\"".$i."\">".$i."</option>";
    }
    echo "<select/>";
    echo "</div>";
  
}


function generateYearsList(){
    echo "<div id=\"yearsfield\" >";
    echo "<h3>Year</h3>";
    echo "<select name=\"years_select\">";
    foreach(range(2015, 1930, -1) as $i){
        echo "<option value=\"".$i."\">".$i."</option>";
    }
    echo "<select/>";
    echo "</div>";
  
}


function generateRuntimeList(){
    echo "<div id=\"runtimeselect\">";
    echo "<h3>Runtime (minute)</h3>";
    echo "<select name=\"runtime_select\">";
    foreach(range(200, 30, -1) as $i){
        echo "<option value=\"".$i."\">".$i."</option>";
    }
    echo "</select>";
    echo "</div>";
}


function generateCountriesList(){
    global $countries;
    echo "<div id=\"countriesselect\">";
    echo "<h3>Country</h3>";
    echo "<select name=\"countries_select\">";
    foreach($countries as $country){
        echo "<option value=\"".$country."\">".$country."</option>";
    }
    echo "</select>";
    echo "</div>";
}

?>

<!DOCTYPE html>

     <html>
     <head>
     <meta charset="UTF-8" >
     <link rel="stylesheet" type="text/css" href="main.css"/>
     <link rel="stylesheet" type="text/css" href="addMovie.css"/>
     <link rel="shortcut icon" href="../ressources/movie_icon.gif"/> <!-- The icon displayed in the tab -->
     <title>Add a movie</title>
     </head>

     <body>
     <?php include_once("header.html"); ?> <!-- We display the header -->

     
    <?php displayAside(); ?> <!-- Displays the list of all genres and years. They are links, 
					   redirecting to the appropriate result page -->
     
    <div id="divform">
    <form name="add" action="actionAddMovie.php" method="POST">
    <div id="titlefield">
      <h3>Title</h3>
      <input type="text" required name="title" placeholder="Title"/>
    </div>
    <div id="releasedatefield">
      <h3>Release date</h3>
      <input type="text" required name="release_date" placeholder="Release date"/>
    </div>
      <?php generateYearsList() ?>
      <?php generateGenresList(); ?>
      <?php generateCountriesList(); ?>

    <?php generateRuntimeList(); ?>
    <div id="directorfield">
      <h3>Director</h3>
      <input type="text" required name="directorFirst" placeholder="First name"/>
      <input type="text" required name="directorLast" placeholder="Last name"/>
    </div>
     <div id="actorsfield">
      <h3>Actors</h3>
      <input required name="actors" placeholder="Ex: Johnny Depp,Brad Pitt,Tom Cruise" />
    </div>
    <div>
      <h3>Synopsis</h3>
      <textarea required name="synopsis" cols="40" rows="10"></textarea>
    </div>
    <div id="posterfield">
      <h3>Poster URL</h3>
      <input required name="posterurl" placeholder="Insert here the poster URL" />
    </div>
    <?php generateRateList() ?>
    <div id="addsubmit">
      <input type="submit" value="Add the film" />
    </div>
    </form>
    </div>


</html>