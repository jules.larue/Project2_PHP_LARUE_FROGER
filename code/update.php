<?php include_once("database_access.php"); // We include the database functions
include_once("countries.php");


// We search the film info in the database, to fill the fields

$res=$connection->query("SELECT * from FILMS natural join GENRES natural join INDIVIDUS where indiv_id=director_id and film_id=".$_GET['id']);
$res=$res->fetch_assoc();

$title=$res['title'];
$genre=$res['name'];
$poster_path=$res['poster_path'];
$directorFirstName=$res['firstname'];
$directorLastName=$res['lastname'];
$year=$res['year'];
$release_date=$res['release_date'];
$runtime=$res['runtime'];
$country=$res['country'];
$synopsis=$res['synopsis'];
$imdb_rating=$res['imdb_rating'];

function genresList(){
    // generates the genre list for the form, but the film genre is the default selected value
    echo "<div id=\"genresfield\">";
    echo "<h3>Genre</h3>";
    echo "<select name=\"genres_select\">";
    global $connection;
    global $genre;
    $result=$connection->query("SELECT name from GENRES ORDER BY name");

    foreach($result as $row){
        if($genre==$row['name'])
            echo "<option selected=\"selected\" value=\"".$row['name']."\">".$row['name']."</option>";
        else
            echo "<option value=\"".$row['name']."\">".$row['name']."</option>";
    }
    echo "</select>";
    echo "</div>";
}

function generateRateList(){
    global $imdb_rating;
    echo "<div id=\"ratefield\" >";
    echo "<h3>Your rate</h3>";
    echo "<select name=\"rate_select\">"; // default selected value is 5
    foreach(range(10, 0, -1) as $i){  
        if($i==$imdb_rating){
            echo "<option selected=\"selected\" value=\"".$i."\">".$i."</option>";
        }
        else
            echo "<option value=\"".$i."\">".$i."</option>";
    }
    echo "<select/>";
    echo "</div>";
  
}


function generateYearsList(){
    global $year;
    echo "<div id=\"yearsfield\" >";
    echo "<h3>Year</h3>";
    echo "<select name=\"years_select\">";
    foreach(range(2015, 1930, -1) as $i){
        if($i==$year)
            echo "<option selected=\"selected\" value=\"".$i."\">".$i."</option>";
        else
            echo "<option value=\"".$i."\">".$i."</option>";
    }
    echo "<select/>";
    echo "</div>";
  
}


function generateRuntimeList(){
    global $runtime;
    echo "<div id=\"runtimeselect\">";
    echo "<h3>Runtime (minute)</h3>";
    echo "<select name=\"runtime_select\">";
    foreach(range(200, 30, -1) as $i){
        if($i==$runtime)
            echo "<option selected=\"selected\" value=\"".$i."\">".$i."</option>";
        else
            echo "<option value=\"".$i."\">".$i."</option>";
    }
    echo "</select>";
    echo "</div>";
}


function generateCountriesList(){
    global $countries;
    global $country;
    echo "<div id=\"countriesselect\">";
    echo "<h3>Country</h3>";
    echo "<select name=\"countries_select\">";
    foreach($countries as $c){
        if($country==$c)
            echo "<option selected=\"selected\" value=\"".$c."\">".$c."</option>";
        else
            echo "<option value=\"".$c."\">".$c."</option>";
    }
    echo "</select>";
    echo "</div>";
}

?>

<!DOCTYPE html>

     <html>
     <head>
     <meta charset="UTF-8" >
     <link rel="stylesheet" type="text/css" href="main.css"/>
     <link rel="stylesheet" type="text/css" href="addMovie.css"/>
     <link rel="stylesheet" type="text/css" href="update.css"/>
     <link rel="shortcut icon" href="../ressources/movie_icon.gif"/> <!-- The icon displayed in the tab -->
     <title>Add a movie</title>
     </head>

     <body>
     <?php include_once("header.html"); ?>


       <?php displayAside(); ?>

    <div id="divform">
    <?php echo "<form name=\"add\" action=\"actionUpdateMovie.php?id=".$_GET['id']."   \" method=\"POST\">";?>
    <div id="titlefield">
      <h3>Title</h3>
      <?php echo "<input type=\"text\" required name=\"title\" value='$title' placeholder=\"Title\"/>"; ?>
    </div>
    <div id="releasedatefield">
      <h3>Release date</h3>
      <?php echo "<input type=\"text\" required name=\"release_date\" value='$release_date' placeholder=\"Release date\"/>"; ?>
    </div>
      <?php generateYearsList() ?>
      <?php genresList(); ?>
      <?php generateCountriesList(); ?>

    <?php generateRuntimeList(); ?>
    <div id="directorfield">
      <h3>Director</h3>
      <?php echo "<input type=\"text\" required name=\"directorFirst\" value='$directorFirstName' placeholder=\"First name\"/>"; ?>
      <?php echo "<input type=\"text\" required name=\"directorLast\" value='$directorLastName' placeholder=\"Last name\"/>"; ?>
    </div>
    <div>
      <h3>Synopsis</h3>
      <?php echo "<textarea required name=\"synopsis\" cols=\"40\" rows=\"10\">$synopsis</textarea>"; ?>
    </div>
    <div id="posterfield">
      <h3>Poster URL</h3>
      <?php echo "<input required name=\"posterurl\" value='$poster_path' placeholder=\"Insert here the poster URL\" />"; ?>
    </div>
    <?php generateRateList() ?>
    <div id="modifysubmit">
      <input type="submit" value="Update it!" />
    </div>
    </form>
    </div>


</html>
