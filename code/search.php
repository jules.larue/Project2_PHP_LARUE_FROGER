<?php include_once("database_access.php"); 


function displaySearchResult(){

    global $connection;
    $search = $_GET['search_bar']; // the user research
    $filter=$_GET['filter'];
    $count=0;
    if($filter=="all"){ // actually, no filter, we serch in everythin
        $condition=" and (LOWER(title) LIKE LOWER('%".$search."%') or LOWER(country) LIKE LOWER('%".$search."%') or LOWER(firstname) LIKE LOWER('%".$search."%') or LOWER(lastname) LIKE LOWER('%".$search."%'))";
    }
    else if($filter=="director"){

        $condition=" and (LOWER(firstname) LIKE LOWER('%".$search."%') or LOWER(lastname) LIKE LOWER('%".$search."%'))";
    }
    else{
        $condition=" and LOWER(".$filter.") LIKE LOWER('%".$search."%')";
    }
    $result=$connection->query("SELECT * from FILMS natural join GENRES natural join INDIVIDUS where indiv_id=director_id ".$condition);

    $num_rows=$result->num_rows;
    if($num_rows==0){
        echo "<p>No result found</p>";
    }

    else{
        echo "<table>";
        echo "<tr>";
        while($row=mysqli_fetch_array($result)){
            if($count%2==0 and $count!=0){
                echo"</tr>";
                echo"<tr>";
            }

            $result_actors = mysqli_query($connection, "SELECT distinct lastname, firstname from ACTEURS natural join INDIVIDUS where indiv_id=ref_id_acteur and ref_id_film=".$row['film_id']);
            $actors=""; // the string displaying the actors
            $i=0;
            foreach($result_actors as $actor){
                if($i==0){
                    $actors=$actor['firstname']." ".$actor['lastname'];
                }
                else{
                    $actors=$actors.", ".$actor['firstname']." ".$actor['lastname'];
                }
                $i=$i+1;
            }

            $movie = new Movie($row['film_id'], $row['title'], $row['name'], $row['poster_path'], $row['firstname']." ".$row['lastname'], $row['year'], $row['release_date'], $row['runtime'], $row['country'], $row['synopsis'], $actors, $row['imdb_rating'], $row['insertion_datetime']);
            echo "<td>";
            displayMovieBlock($movie);
            echo "</td>";
            $count=$count+1;
        }
        echo "</tr>";
        echo "</table>";
    }
}
?> 

<!DOCTYPE HTML>
    <!-- This is the homepage of the website, where we can see the latest films added. -->
     <html>
     <head>
     <meta charset="UTF-8" >
     <link rel="stylesheet" type="text/css" href="main.css"/>
     <link rel="shortcut icon" href="../ressources/movie_icon.gif"/> <!-- The icon displayed in the tab -->
     <title>The web films collection</title>
     </head>

     <body>
     <?php include_once("header.html"); ?> <!-- We display the header -->


       <?php displayAside(); ?>


     <section id="main_section">
       <h2>Search results</h2>
       <?php displaySearchResult(); ?>
     </section>
     </body>
     </html>