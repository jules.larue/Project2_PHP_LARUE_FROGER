<?php

include_once("Function_Movie_Block.php");
include_once("Movie.php");

// We create the connection to the MySQL database
$connection = new mysqli('servinfo-db', 'dblarue', 'larue', 'dblarue');   

function displayAside(){
	/* 
		Will display HTML code, with the list of
		all the genres and all the years, that will be displayed in a
		"aside" tag (we don't display the aside tag here,
		it's already done in htl files.)
	*/
	global $connection; // we get the connection created at the beginning
	$resultGenre = $connection->query("SELECT name from GENRES ORDER BY name"); // the result of the query
	

    echo "<aside id=\"searchByCriteria\">";

    /* For the genre */
    echo "<h3>Genres</h3>";

	echo "<ul>\n   ";
	foreach($resultGenre as $row){
		echo "<li><a href=\"searchFilter.php?genre=".$row['name']."\">".ucfirst($row['name'])."</a></li>\n";
		// example : <li><a href="searchResult.php?filter=action">Action</a> <--- Link to action films only
	}
	echo "</ul>\n"; // end of the list

    /* Now for the years */
    $resultYear = $connection->query("SELECT distinct year from FILMS ORDER BY year DESC"); // select all years stored from greater to lower (each year appears only once in result)
	
    echo "<h3>Years</h3>";
    echo "<ul>\n";
    foreach($resultYear as $row){
        echo "<li><a href=\"searchFilter.php?year=".$row['year']."\">".intval($row['year'])."</a></li>\n";
        // example : <li><a href="searchResult.php?filter=action">Action</a> <--- Link to action films only
    }
    echo "</ul>\n"; // end of the list

    echo "</aside>";
}

function displayLastAddedFilms(){
	$count=0; // when reach 20, we stop, to not display too much films

	global $connection;

	echo "<table>\n";
	echo "<tr>\n";
	$result = $connection->query("SELECT * from FILMS natural join GENRES natural join INDIVIDUS where indiv_id=director_id ORDER BY insertion_datetime DESC");
    if(count($result)==0){ // no result found
        echo "<p>No result found</p>\n";
    }
    else{
	   while($row=mysqli_fetch_array($result) and $count<20){
		  if($count%2==0 and $count!=0){
		  	echo"</tr>\n";
		    echo"<tr>\n";
		}

		$result_actors = mysqli_query($connection, "SELECT distinct lastname, firstname from ACTEURS natural join INDIVIDUS where indiv_id=ref_id_acteur and ref_id_film=".$row['film_id']);
		$actors=""; // the string displaying the actors
		$i=0;
		foreach($result_actors as $actor){
			if($i==0){
				$actors=$actor['firstname']." ".$actor['lastname'];
			}
			else{
				$actors=$actors.", ".$actor['firstname']." ".$actor['lastname'];
			}
			$i=$i+1;
		}


		$movie = new Movie($row['film_id'], $row['title'], $row['name'], $row['poster_path'], $row['firstname']." ".$row['lastname'], $row['year'], $row['release_date'], $row['runtime'], $row['country'], $row['synopsis'], $actors, $row['imdb_rating'], $row['insertion_datetime']);
		echo "<td>\n";
		displayMovieBlock($movie);
		echo "</td>\n";
		$count=$count+1;
	}
	echo "</tr>\n";
	echo "</table>\n";
}
}





function displaySearchFilterResult(){

    global $connection;
    if(isset($_GET['genre'])){
        $filter="name"; // the genre chosen by the user
        $value="'".$_GET['genre']."'"; // we must add quotes for SQL syntax
    }
    else{ // we chose year
        $filter="year";
        $value=$_GET['year'];
    }

    $result=$connection->query("SELECT * from FILMS natural join GENRES natural join INDIVIDUS where indiv_id=director_id and ".$filter."=$value ORDER BY title");
    $count=0;
    $num_rows=$result->num_rows;
    if($num_rows==0){
        echo "<p>No result found</p>";
    }

    else{
        echo "<table>";
        echo "<tr>";
        while($row=mysqli_fetch_array($result)){
            if($count%2==0 and $count!=0){
                echo"</tr>";
                echo"<tr>";
            }

            $result_actors = mysqli_query($connection, "SELECT distinct lastname, firstname from ACTEURS natural join INDIVIDUS where indiv_id=ref_id_acteur and ref_id_film=".$row['film_id']);
            $actors=""; // the string displaying the actors
            $i=0;
            foreach($result_actors as $actor){
                if($i==0){
                    $actors=$actor['firstname']." ".$actor['lastname'];
                }
                else{
                    $actors=$actors.", ".$actor['firstname']." ".$actor['lastname'];
                }
                $i=$i+1;
            }

            $movie = new Movie($row['film_id'], $row['title'], $row['name'], $row['poster_path'], $row['firstname']." ".$row['lastname'], $row['year'], $row['release_date'], $row['runtime'], $row['country'], $row['synopsis'], $actors, $row['imdb_rating'], $row['insertion_datetime']);
            echo "<td>";
            displayMovieBlock($movie);
            echo "</td>";
            $count=$count+1;
        }
        echo "</tr>";
        echo "</table>";
    }
}

function generateGenresList(){
    echo "<div id=\"genresfield\">";
    echo "<h3>Genre</h3>";
    echo "<select name=\"genres_select\">";
    global $connection;
    $result=$connection->query("SELECT name from GENRES ORDER BY name");

    foreach($result as $row){
        echo "<option value=\"".$row['name']."\">".$row['name']."</option>";
    }
    echo "</select>";
    echo "</div>";
}

/*

THE TWO FOLLOWING FUNCTIONS COULD HAVE BEEN USED, THEY WORK, BUT AT THE IUT, THE CURL CAN'T ACCESS THE URL CONTENT,
PROBABLY BECAUSE OF PROXY PARAMS.
INSTEAD OF THIS, THE USER ENTERS MANUALLY THE POSTER PATH AND THE ACTORS


function getPosterPathAndRate($title, $year){
    //returns the poster and imdb rate with title and year of a film. If film not found, returns an empty string.
    $res = array(); // a json array got from omdbapi.com
    $title=str_replace(" ", "+", $title);
    
    $url = 'http://www.omdbapi.com/?t='.$title.'&y='.$year.'&plot=full&r=json';
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl,CURLOPT_TIMEOUT,1000);
    $array = json_decode(curl_exec($curl), true);
    if(isset($array['Poster'])) // some films don't have posters on omdbapi.com
            $res['Poster']=$array['Poster'];
    else
        $res['Poster']="";
        
    $res['imdbRating']=$array['imdbRating'];

    return $res;
}

function getActors($title, $year){
    // returns an array of the actors of the film, an empty array if not found or error
    $title=str_replace(" ", "+", $title);
    $url = 'http://www.omdbapi.com/?t='.$title.'&y='.$year.'&plot=full&r=json';
    
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl,CURLOPT_TIMEOUT,1000);
    $array = json_decode(curl_exec($curl), true);
    if($array['Actors']!=null and $array['Actors']!="N/A"){
        return explode(', ', $array['Actors']);
    }
}

*/


function getDirectorId($directorFirst, $directorLast){
    global $connection;
    $result=$connection->query("SELECT indiv_id from INDIVIDUS where lastname='".$directorLast."' and firstname='".$directorFirst."'");
    $id=$result->fetch_assoc();
    return $id['indiv_id'];
}




function handleDirector($directorFirst, $directorLast){ // ex: "Steven Spielberg" - "Leonardo di Caprio"
    global $connection;     
    $result=$connection->query("SELECT * from FILMS natural join INDIVIDUS where indiv_id=director_id and firstname='$directorFirst' and lastname='$directorLast'");
    // If result empty, we insert the director; else do nothing
    $num_rows=$result->num_rows;
    echo "1";
    if($num_rows==0){ // director does not exist : we insert it
         echo "2";   
         echo "INSERT into INDIVIDUS (`lastname`, `firstname`) values ('$directorLast', '$directorFirst')";
         $connection->query("INSERT into INDIVIDUS (`lastname`, `firstname`) values ('$directorLast', '$directorFirst')");
    }
}




function handleActors($actors){
    /*
      ONLY for the cases where firstname & lastname = each one word
    */
    global $connection;
    foreach($actors as $actor){
        $array=explode(" ", $actor); // [firstname, lastname]
        $firstname=$array[0];
        $lastname=$array[1];
        $result=$connection->query("SELECT * from INDIVIDUS where lastname='$lastname' and firstname='$firstname'");
        $num_rows=$result->num_rows;
        if($num_rows==0){ // actor does not exist; we insert him/her
            $connection->query("INSERT into INDIVIDUS (`lastname`, `firstname`) values ('$lastname', '$firstname')");
        }
    }
}


function filmAlreadyExists($title,$year){
    // returns if the couple (title, year) exists in database
    global $connection;
    echo "SELECT * from FILMS where title='$title' and year=$year";
    $result=$connection->query("SELECT * from FILMS where title='$title' and year=$year");
    $num_rows=$result->num_rows;
    if($num_rows==0){ // result empty : film does not exist
        return false;
    }
    return true;
}


function getGenreId($gname){
    // get the id of a genre from its name
    global $connection;
    $result=$connection->query("SELECT genre_id from GENRES where name='".$gname."'");
    $name=$result->fetch_assoc();
    return $name['genre_id'];
}



function getActorId($firstname, $lastname){
    global $connection;
    $result=$connection->query("SELECT indiv_id from INDIVIDUS where firstname='$firstname' and lastname='$lastname'");
    $id=$result->fetch_assoc();
    return $id['indiv_id'];
}




function insertNewFilm($title, $release_date, $year, $genre, $country, $runtime, $directorFirst,$directorLast, $actors, $synopsis, $posterPath, $rate){

    global $connection;
    $title=str_replace("'", "\'", $title);
    $synopsis=str_replace("'", "\'", $synopsis);


    $actors=explode(",", $actors); // the array of actors
    
    handleDirector($directorFirst, $directorLast); // insert director if does not exist
    handleActors($actors); // insert actors that are not in the database

    if(!filmAlreadyExists($title, $year)){ // if film does no exist

        // we insert it in database
        echo "'$title', '$country', $year, '$release_date', $runtime, NOW(), '$synopsis', ".getDirectorId($directorFirst, $directorLast).", ". getGenreId($genre).", '".$posterPath."', ".$rate.")";
        $connection->query("INSERT into FILMS (`title`, `country`, `year`, `release_date`, `runtime`, `insertion_datetime`, `synopsis`, `director_id`, `genre_id`, `poster_path`, `imdb_rating`)
                                     values ('$title', '$country', $year, '$release_date', $runtime, NOW(), '$synopsis', ".getDirectorId($directorFirst, $directorLast).", ". getGenreId($genre).", '".$posterPath."', ".$rate.")");
        


        $result=$connection->query('SELECT MAX(film_id) as max from FILMS');
        $max=$result->fetch_assoc();
        foreach($actors as $actor){ // we link each actor to the film
            $arraynames=explode(" ", $actor); // we split firstname and lastname in an array
            $connection->query("INSERT into ACTEURS values(".$max['max'].", " .getActorId($arraynames[0], $arraynames[1]).")");
        }

        return true; // we indicate that everything happened well.
    }

    else{
        // we indicate there is an error : film already exists in the database...
    }
}




function displayInfosSection(){
    // displays the info of a movie
    global $connection;
    $film_id=$_GET['film_id'];

    $result=mysqli_query($connection, "SELECT * from FILMS natural join INDIVIDUS natural join GENRES where director_id=indiv_id and film_id=".$film_id);
    $result=$result->fetch_assoc(); // there will always be only one result
    

    /* We create the string containing the actors of the film */
    $result_actors = $connection->query("SELECT distinct lastname, firstname from ACTEURS natural join INDIVIDUS where indiv_id=ref_id_acteur and ref_id_film=".$result['film_id']);
    $actors=""; // the string displaying the actors
    $i=0;
    foreach($result_actors as $actor){
        if($i==0){
            $actors=$actor['firstname']." ".$actor['lastname'];
        }
        else{
            $actors=$actors.", ".$actor['firstname']." ".$actor['lastname'];
        }
        $i=$i+1;
    }

    // Now we have all the infos, we can display them

    echo "<img src=\"".$result['poster_path']."\" />"; // we display the poster

    // we display the release date, runtime and country under the poster

    echo "<table id=\"undertable\">";
    echo "<tr>";
    echo "<th>Released : </th>";
    echo "<td>".$result['release_date'];"</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<th>Runtime : </th>";
    echo "<td>".$result['runtime'] ." minutes</td>";
    echo "</tr>";
    
    echo "<tr>";
    echo "<th>Country : </th>";
    echo "<td>".$result['country']."</td>";
    echo "</tr>";
    echo "</table>";


    // At the right of the poster, we display a block with the other infos (title, director, genre, synopsis, etc)

    echo "<section id=\"rightblock\">";
    echo "<h2>".$result['title']." (".$result['year'].")</h2>";

    echo "<table id=\"righttable\">";
    echo "<tr>";
    echo "<th>Genre : </th>";
    echo "<td>".strval($result['name'])."</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<th>Rate : </th>";
    echo "<td>".strval($result['imdb_rating'])." / 10</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<th>Directed by : </th>";
    echo "<td>".$result['firstname']." ". $result['lastname']."</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<th>Cast : </th>";
    echo "<td>".$actors."</td>";
    echo "</tr>";
    echo "</table>";

    echo "<div id=\"synopsisDiv\">";
    echo "<h3 id=\"synopsis\">Synopsis</h3>";
    echo "<p>".$result['synopsis']."</p>";
    echo "</div>";

    echo "</section>";

    // We display two buttons to remove and edit a film
    echo "<a id=\"remove\" href='remove.php?id=".$result['film_id']."'>Remove film</a>";
    echo "<a id=\"update\" href='update.php?id=".$result['film_id']."'>Modify film</a>";
}



function removeFilm($id){
    global $connection;

    $connection->query("DELETE from ACTEURS where ref_id_film=$id");
    $connection->query("DELETE from FILMS where film_id=$id");
}

 
?>
